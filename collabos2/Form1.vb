﻿Public Class Form1


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ListBox1.Items.Clear()
        'again, lets first check if entered text is actually a number
        If Not IsNumeric(TextBox1.Text) Then
            MsgBox(TextBox1.Text & " is not a number!")
        Else
            'not the best implementation
            'but i am bad at math, so i had to google a lot on how factors work
            If Val(TextBox1.Text) > 0 Then
                Dim rez As New List(Of Double)
                Dim inp As Double = Val(TextBox1.Text)
                Dim fac As Double = 2
                Do Until inp = 1
                    For fac = 2 To inp Step 1
                        If inp Mod fac = 0 Then
                            rez.Add(fac)
                            inp = inp / fac
                            GoTo Final
                        End If
                    Next
Final:
                Loop
                For Each item As Double In rez
                    ListBox1.Items.Add(item.ToString)
                Next
            Else
                MsgBox("Number has to be bigger than 0")
            End If
        End If
    End Sub
End Class
